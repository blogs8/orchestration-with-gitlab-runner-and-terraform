INSTALLING GITLAB IN AWS EC2 WITH GITLAB RUNNER AND TERRAFORM

A step-by-step process for installing GITLAB in AWS ec2.

As we know GitLab is open source, you can create an account at https://gitlab.com/ and design your own orchestration as a Source Control Management and Continuous Integration.

Here we create our own GITLAB setup.

Prerequisite:

AWS account
Putty
Stay Calm
Let Start

Step 1
Login to your AWS account, and move to the AWS marketplace(https://aws.amazon.com/marketplace).

GitLab is free for the community edition. Head to Amazon’s Marketplace and get the GitLab Community Edition.


you need to Continue to Subscribe and Launch an instance.

At least select the t2.medium instance type


Once Ec2 launches assign the EIP to Ec2. Reason for EIP that need static IP to define into the GitLab runner.

/


You can get an option of EIP in the EC2 dashboard. Click in the EIP


Allocate the Elastic IP address and associate EIP with Ec2.

Step2
Open the Putty, Use EIP for Login and key for Auth


ubuntu is the user name for Ec2


Once you login the Ec2, need to Update the Ubuntu by running the following command

sudo apt update

sudo apt dist-upgrade

sudo do-release-upgrade -d

While upgrading in a few points ask for your input, make sure to press Y / yes / enter


Enter Y

It’s going to take few minutes


select the install the package maintainer’s version and Enter
Once Upgrade is done, check the release

lsb_release -a


Now configured the GITLAB in ec2

sudo gitlab-ctl reconfigure

sudo apt-get update

sudo apt-get upgrade

sudo gitlab-ctl restart

After finishing this config, it’s time to log in to the GitLab


Use Http

http://ec2-44-205-108-164.compute-1.amazonaws.com/users/sign_in

The next step is to reset the root password of GitLab

sudo su root

sudo gitlab-rake “gitlab:password:reset”

Note before login into Gitlab needs to check the gitlab.rb file. make EIP is updated in gitla.rb file
/etc/gitlab/gitlab.rb


The default IP config in .rb file, change IP with EIP
After changing the IP run the config command again

sudo gitlab-ctl reconfigure

STEP 3
Install the GITLAB RUNNER in the same Ec2. The Runner is for Continuous Integration.

sudo apt-get update

curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

sudo apt-get install gitlab-runner

sudo gitlab-runner register

The register command asked for the input URL. Enter your GITLAB login URL

Foe token, go to the admin, select the runner option




Enter shell as an executor


sudo gitlab-runner start

root@ip-172-31-17-195:/etc/gitlab# sudo gitlab-runner start
Runtime platform arch=amd64 os=linux pid=9938 revision=febb2a09 version=15.0.0

STEP 4
Installing the terraform in the same Ec2 for IaaC.

sudo apt update

sudo apt install wget unzip

Then download the latest terraform archive.

TER_VER=`curl -s https://api.github.com/repos/hashicorp/terraform/releases/latest | grep tag_name | cut -d: -f2 | tr -d \”\,\v | awk ‘{$1=$1};1’` wget https://releases.hashicorp.com/terraform/${TER_VER}/terraform_${TER_VER}_linux_amd64.zip

unzip terraform_${TER_VER}_linux_amd64.zip

sudo mv terraform /usr/local/bin/

which terraform

terraform version

root@ip-172-31-17-195:~# terraform version
Terraform v1.2.2
on linux_amd64
root@ip-172-31-17-195:~#

Now we are ready for our complete orchestration.

We have SCM(GITLAB) for the code repository, Continuous Integration is Runner, and Terraform for configuration.

With this setup, you can manage multiple Environments like AWS, GCP, Azure, and many more…

Hope you like this page.

Welcome to your feedback.
